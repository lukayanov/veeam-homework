namespace Veeam.HomeWork.Compressor
{
    public class CompressorOptions
    {
        public int ProcessorThreadsCount { get; set; }
        public int BlockSize { get; set; }
        public int MaxMessagesInQueues { get; set; }
    }
}
using System.Threading;

namespace Veeam.HomeWork.Decompressor
{
    public interface IDecompressor
    {
        bool DeCompress(string inputFile, string outputFile, CancellationToken token);
    }
}
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Veeam.HomeWork.Common.Pipeline.Queues
{
    // Thread safe queue with signaling object and backpressure support
    public sealed class SafeQueue<T> : IQueue<T> where T : class
    {
        private readonly IItemsCounter _counter;
        private readonly Queue<T> _queue = new Queue<T>();
        private readonly Semaphore _semaphore = new Semaphore(0, int.MaxValue);
        private readonly object _sync = new object();

        public SafeQueue(IItemsCounter counter)
        {
            _counter = counter;
        }

        public WaitHandle Event => _semaphore;

        public void Push(T item)
        {
            lock (_sync)
            {
                _queue.Enqueue(item);
                _semaphore.Release();
                _counter.Increment();
            }
        }

        public T Get()
        {
            lock (_sync)
            {
                if (!_queue.Any())
                {
                    return null;
                }

                var result = _queue.Dequeue();
                _counter.Decrement();
                return result;
            }
        }

        // For debugging purpose only
        public override string ToString()
        {
            int queueLength;
            lock (_sync)
            {
                queueLength = _queue.Count;
            }

            return $"{queueLength} elements";
        }
    }
}
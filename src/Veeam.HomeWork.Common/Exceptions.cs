using System;

namespace Veeam.HomeWork.Common
{
    public class ReaderException : Exception
    {
        public ReaderException(Exception inner)
        {
            Inner = inner;
        }

        public Exception Inner { get; }
    }

    public class ProcessorException : Exception
    {
        public ProcessorException(Exception inner)
        {
            Inner = inner;
        }

        public Exception Inner { get; }
    }

    public class WriterException : Exception
    {
        public WriterException(Exception inner)
        {
            Inner = inner;
        }

        public Exception Inner { get; }
    }
}
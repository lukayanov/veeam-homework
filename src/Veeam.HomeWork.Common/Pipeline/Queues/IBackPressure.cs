using System.Threading;

namespace Veeam.HomeWork.Common.Pipeline.Queues
{
    public interface IItemsCounter
    {
        void Increment();
        void Decrement();
    }

    public interface IBackPressure
    {
        public WaitHandle Event { get; }
    }
}
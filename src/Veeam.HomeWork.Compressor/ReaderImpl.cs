using System.IO;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Compressor
{
    // Reader class implementation, reads input file by chunks of size defined by blockSize argument
    // Empty input files are considered as bad input 
    public class ReaderImpl : IGenericProcessor<int, WorkItem>
    {
        private readonly long _blockSize;
        private readonly FileStream _stream;
        private readonly long _totalCount;
        private long _bytesToRead;

        public ReaderImpl(string fileName, long blockSize)
        {
            _blockSize = blockSize;
            _stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            _bytesToRead = _stream.Length;
            if (_bytesToRead == 0)
            {
                throw new FileLoadException("Input file has must have some content.");
            }

            _totalCount = _bytesToRead / blockSize + (_bytesToRead % blockSize != 0 ? 1 : 0);
        }

        public void Dispose()
        {
            _stream?.Dispose();
        }

        public WorkItem Process(int index)
        {
            if (_bytesToRead == 0)
            {
                return null;
            }

            var block = new byte[_bytesToRead >= _blockSize ? _blockSize : _bytesToRead];
            _bytesToRead -= _stream.Read(block);
            var workItem = new WorkItem(index, _totalCount, _bytesToRead == 0, block);
            return workItem;
        }
    }
}
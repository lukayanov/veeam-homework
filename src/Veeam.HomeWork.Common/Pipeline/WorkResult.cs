using System;

namespace Veeam.HomeWork.Common.Pipeline
{
    // Processor result containing produced WorkItem and potentially any exception raised while processing 
    public class WorkResult<T> where T : class
    {
        public WorkResult(Exception ex)
        {
            Item = null;
            Ex = ex;
        }

        public WorkResult(T item)
        {
            Item = item;
            Ex = null;
        }

        public T Item { get; }
        public Exception Ex { get; }
    }
}
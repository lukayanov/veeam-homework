using System.Collections.Generic;
using System.IO;
using Serilog;
using Veeam.HomeWork.Common.FileStructure;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Compressor
{
    // Writes compressed blocks to the file.
    // File structure does conform to gz standard, see read.me file
    public class WriterImpl : IGenericProcessor<WorkItem, WorkItem>
    {
        private readonly List<FileItem> _fileBlocks = new List<FileItem>();
        private readonly string _fileName;
        private readonly ILogger _logger = Log.ForContext<WriterImpl>();
        private readonly FileStream _stream;
        private bool _finalBlockReceived;

        public WriterImpl(string fileName)
        {
            _fileName = fileName;
            _stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read);
            _stream.WriteFileHeader(new FileItem()); // reserve space for header 
        }

        public void Dispose()
        {
            if (!_finalBlockReceived)
            {
                _stream?.Dispose();
                _logger.Debug("Final block is not received before disposing, output file will be removed");
                File.Delete(_fileName);
                _logger.Debug("File is deleted");
            }
            else
            {
                var footerOffset = _stream.Position;
                var footerLength = _stream.WriteFileFooter(_fileBlocks);

                _stream.Seek(0, SeekOrigin.Begin); // Write real header
                _stream.WriteFileHeader(new FileItem {Offset = footerOffset, Length = footerLength});

                _stream?.Dispose();
            }
        }

        public WorkItem Process(WorkItem item)
        {
            _fileBlocks.Add(new FileItem {Offset = _stream.Position, Length = item.Block.Length});
            _stream.Write(item.Block);
            if (item.IsFinal)
            {
                _logger.Debug("Final block is received");
                _finalBlockReceived = true;
            }

            return new WorkItem(item.SerialNumber, item.TotalCount, item.IsFinal);
        }
    }
}
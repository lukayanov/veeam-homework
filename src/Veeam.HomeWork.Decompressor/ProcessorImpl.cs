using System.IO;
using System.IO.Compression;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Decompressor
{
    // DeCompression processor, decompresses one byte array and stores it to another 
    public class ProcessorImpl : IGenericProcessor<WorkItem, WorkItem>
    {
        public WorkItem Process(WorkItem item)
        {
            using var inputStream = new MemoryStream(item.Block);
            using var outputStream = new MemoryStream();
            using (var decompressionStream = new GZipStream(inputStream, CompressionMode.Decompress))
            {
                decompressionStream.CopyTo(outputStream);
            }

            return new WorkItem(item.SerialNumber, item.TotalCount, item.IsFinal, outputStream.ToArray());
        }

        public void Dispose()
        {
            // nothing to dispose here 
        }
    }
}
using System;
using System.Threading;
using Microsoft.Extensions.Options;
using Serilog;
using Veeam.HomeWork.Common;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Compressor
{
    public class Compressor : ICompressor
    {
        private readonly ILogger _logger = Log.ForContext<Compressor>();
        private readonly CompressorOptions _options;

        public Compressor(IOptions<CompressorOptions> options)
        {
            _options = options.Value;
        }

        // This method initiates the pipeline with reader, processor and writer implementing compression functionality 
        public bool Compress(string inputFile, string outputFile, CancellationToken token)
        {
            try
            {
                _logger.Debug("Building the pipeline");
                var processingPipeline = new Pipeline(() => new ReaderImpl(inputFile, _options.BlockSize), () => new ProcessorImpl(), () => new WriterImpl(outputFile)
                    , _options.ProcessorThreadsCount, _options.MaxMessagesInQueues
                    , progress => _logger.Information($"Current progress is {progress}%."));
                _logger.Information("Starting compression process.");
                processingPipeline.Run(token);
                _logger.Information("Compression process is completed successfully.");
                return true;
            }
            catch (OperationCanceledException)
            {
                _logger.Error("Operation is cancelled by the user");
            }
            catch (ReaderException e)
            {
                _logger.Error(e.Inner, $"Exception has occured while reading the input file {inputFile}");
            }
            catch (ProcessorException e)
            {
                _logger.Error(e.Inner, "Exception has occured while compressing the data stream");
            }
            catch (WriterException e)
            {
                _logger.Error(e.Inner, $"Exception has occured while writing the file {outputFile}");
            }
            catch (Exception e)
            {
                _logger.Error(e, "Unexpected error has happened");
            }

            return false;
        }
    }
}
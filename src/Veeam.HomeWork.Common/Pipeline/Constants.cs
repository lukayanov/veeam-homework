using System;

namespace Veeam.HomeWork.Common.Pipeline
{
    public static class Constants
    {
        public static TimeSpan Timeout => TimeSpan.FromSeconds(10);
    }
}
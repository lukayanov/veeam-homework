using FluentAssertions;
using Veeam.HomeWork.Common.Pipeline.Queues;
using Xunit;

namespace Veeam.HomeWork.Common.Tests.Queues
{
    public class BackPressureCounterTests
    {
        [Fact]
        public void BP_is_enabled_by_default()
        {
            var bp = new BackPressureCounter(1);
            bp.Event.WaitOne(0).Should().BeTrue();
        }

        [Fact]
        public void Increment_resets_event()
        {
            var bp = new BackPressureCounter(2);
            bp.Event.WaitOne(0).Should().BeTrue();
            bp.Increment();
            bp.Event.WaitOne(0).Should().BeTrue();
            bp.Increment();
            bp.Event.WaitOne(0).Should().BeFalse();
        }

        [Fact]
        public void Decrement_enables_push()
        {
            var bp = new BackPressureCounter(2);
            bp.Event.WaitOne(0).Should().BeTrue();
            bp.Increment();
            bp.Event.WaitOne(0).Should().BeTrue();
            bp.Increment();
            bp.Event.WaitOne(0).Should().BeFalse();
            bp.Decrement();
            bp.Event.WaitOne(0).Should().BeTrue();
            bp.Decrement();
            bp.Event.WaitOne(0).Should().BeTrue();
        }
    }
}
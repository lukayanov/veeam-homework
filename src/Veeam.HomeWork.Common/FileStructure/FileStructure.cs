using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace Veeam.HomeWork.Common.FileStructure
{
    public static class FileStructure
    {
        public static FileItem ReadFileHeader(this Stream stream)
        {
            return ReadFileItem(stream);
        }

        public static void WriteFileHeader(this Stream stream, FileItem footerLocation)
        {
            WriteFileItem(stream, footerLocation);
        }

        public static IEnumerable<FileItem> ReadFileFooter(this Stream stream, long length)
        {
            var sizeOf = Marshal.SizeOf(typeof(FileItem));
            for (var i = 0; i < length / sizeOf; ++i)
            {
                yield return ReadFileItem(stream);
            }
        }

        public static int WriteFileFooter(this Stream stream, IEnumerable<FileItem> footerContent)
        {
            var counter = 0;
            foreach (var item in footerContent)
            {
                counter += WriteFileItem(stream, item);
            }

            return counter;
        }

        private static FileItem ReadFileItem(Stream stream)
        {
            var size = Marshal.SizeOf(typeof(FileItem));
            var bytes = new byte[size];
            if (stream.Read(bytes, 0, size) != size)
            {
                throw new FileLoadException("Bad file structure");
            }

            var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            try
            {
                var item = Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(FileItem));
                if (item == null)
                {
                    throw new FileLoadException("Bad file structure");
                }

                return (FileItem) item;
            }
            finally
            {
                handle.Free();
            }
        }

        private static int WriteFileItem(Stream stream, FileItem item)
        {
            var size = Marshal.SizeOf(typeof(FileItem));
            var ptr = Marshal.AllocHGlobal(size);
            try
            {
                Marshal.StructureToPtr(item, ptr, true);
                var buffer = new byte[size];
                Marshal.Copy(ptr, buffer, 0, size);
                stream.Write(buffer);
                return size;
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
        }
    }
}
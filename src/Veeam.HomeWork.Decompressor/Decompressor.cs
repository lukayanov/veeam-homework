using System;
using System.Threading;
using Microsoft.Extensions.Options;
using Serilog;
using Veeam.HomeWork.Common;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Decompressor
{
    public class Decompressor : IDecompressor
    {
        private readonly ILogger _logger = Log.ForContext<Decompressor>();
        private readonly DecompressorOptions _options;

        public Decompressor(IOptions<DecompressorOptions> options)
        {
            _options = options.Value;
        }

        // This method initiates the pipeline with reader, processor and writer implementing decompression functionality 
        public bool DeCompress(string inputFile, string outputFile, CancellationToken token)
        {
            try
            {
                _logger.Debug("Building the pipeline");
                var processingPipeline = new Pipeline(() => new ReaderImpl(inputFile), () => new ProcessorImpl(), () => new WriterImpl(outputFile)
                    , _options.ProcessorThreadsCount, _options.MaxMessagesInQueues
                    , progress => _logger.Information($"Current progress is {progress}%."));

                _logger.Information("Starting decompression process.");
                processingPipeline.Run(token);
                _logger.Information("Decompression process is completed successfully.");
                return true;
            }
            catch (OperationCanceledException)
            {
                _logger.Error("Operation is cancelled by the user");
            }
            catch (ReaderException e)
            {
                _logger.Error(e.Inner, $"Exception has occured while reading the input file {inputFile}");
            }
            catch (ProcessorException e)
            {
                _logger.Error(e.Inner, "Exception has occured while decompressing the data stream");
            }
            catch (WriterException e)
            {
                _logger.Error(e.Inner, $"Exception has occured while writing the file {outputFile}");
            }
            catch (Exception e)
            {
                _logger.Error(e, "Unexpected error has happened");
            }

            return false;
        }
    }
}
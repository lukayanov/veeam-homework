using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Veeam.HomeWork.Common.Pipeline.Queues
{
    // Orders the incoming items by the long key in ascending order.
    // Items become available in the queue only when they build an uninterrupted sequence started by currently awaited key
    // Queue provides signaling object and backpressure support
    public sealed class OrderedSafeQueue<T> : IQueue<T> where T : class
    {
        private readonly IItemsCounter _backPressureCounter;
        private readonly SortedList<long, T> _inputQueue = new SortedList<long, T>();
        private readonly Func<T, long> _keyExtractor;
        private readonly List<T> _readyQueue = new List<T>();
        private readonly Semaphore _semaphore = new Semaphore(0, int.MaxValue);
        private readonly object _sync = new object();
        private int _awaitedKey;

        public OrderedSafeQueue(Func<T, long> keyExtractor, IItemsCounter backPressureCounter, int startKey)
        {
            _awaitedKey = startKey;
            _keyExtractor = keyExtractor;
            _backPressureCounter = backPressureCounter;
        }

        public WaitHandle Event => _semaphore;

        public void Push(T item)
        {
            lock (_sync)
            {
                _inputQueue.Add(_keyExtractor(item), item);
                TryPromote();
                _backPressureCounter.Increment();
            }
        }

        public T Get()
        {
            lock (_sync)
            {
                if (!_readyQueue.Any())
                {
                    return null;
                }

                var result = _readyQueue[0];
                _readyQueue.RemoveAt(0);
                _backPressureCounter.Decrement();
                return result;
            }
        }

        private void TryPromote()
        {
            if (!_inputQueue.Any() || _inputQueue.Keys[0] != _awaitedKey)
            {
                return;
            }

            var count = 0;
            for (; _inputQueue.Any() && _inputQueue.Keys[0] == _awaitedKey; ++_awaitedKey)
            {
                _readyQueue.Add(_inputQueue.Values[0]);
                _inputQueue.RemoveAt(0);
                count++;
            }

            _semaphore.Release(count);
        }

        // For debugging purpose only
        public override string ToString()
        {
            int inputQueueLength;
            int readyQueueLength;
            lock (_sync)
            {
                inputQueueLength = _inputQueue.Count;
                readyQueueLength = _readyQueue.Count;
            }

            return $"{inputQueueLength} unordered elements,{readyQueueLength} ordered elements";
        }
    }
}
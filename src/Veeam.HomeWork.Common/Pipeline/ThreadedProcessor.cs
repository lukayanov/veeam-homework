using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Serilog;
using Veeam.HomeWork.Common.Pipeline.Queues;

namespace Veeam.HomeWork.Common.Pipeline
{
    // This object runs a single IGenericProcessor on a separate thread
    public class ThreadedProcessor<TInput, TOutput> where TOutput : class
    {
        private static readonly ILogger Logger = Log.ForContext<ThreadedProcessor<TInput, TOutput>>();
        private readonly ManualResetEvent _stopEvent = new ManualResetEvent(false);
        private readonly List<Thread> _threads;

        public ThreadedProcessor(ISource<TInput> source, ISink<WorkResult<TOutput>> sink, Func<IGenericProcessor<TInput, TOutput>> processorsFactory, int processorsCount)
        {
            _threads = Enumerable.Range(0, processorsCount).Select(i => new Thread(obj => DoWork(source, sink, processorsFactory, _stopEvent))).ToList();
        }

        public IDisposable Start()
        {
            _threads.ForEach(t => t.Start());
            return new DisposeAction(Stop);
        }

        private void Stop()
        {
            Logger.Debug("Stopping processor");

            _stopEvent.Set();

            Logger.Debug("Joining threads");
            foreach (var thread in _threads)
            {
                if (!thread.Join(Constants.Timeout))
                {
                    Logger.Warning("Join is aborted by timeout");
                }
            }

            Logger.Debug("Threads are joined");
        }

        private static void DoWork(ISource<TInput> source, ISink<WorkResult<TOutput>> sink, Func<IGenericProcessor<TInput, TOutput>> processorsFactory, WaitHandle stopEvent)
        {
            try
            {
                using var processor = processorsFactory();
                while (true)
                {
                    var waitResult = WaitHandle.WaitAny(new[] {stopEvent, source.Event}, Constants.Timeout);
                    if (waitResult == WaitHandle.WaitTimeout)
                    {
                        throw new TimeoutException("Timeout expired waiting for source event.");
                    }

                    if (waitResult == 0)
                    {
                        Logger.Debug("Stop signal is received");
                        return; // Stop event is signaled
                    }

                    if (waitResult == 1)
                    {
                        var item = source.Get();
                        var outputItem = processor.Process(item);
                        if (outputItem != null)
                        {
                            sink.Push(new WorkResult<TOutput>(outputItem));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                sink.Push(new WorkResult<TOutput>(ex));
            }
        }
    }
}
using System.Threading;

namespace Veeam.HomeWork.Common.Pipeline.Queues
{
    public interface IQueue<T> : ISource<T>, ISink<T>
    {
    }

    public interface ISource<out T>
    {
        public WaitHandle Event { get; }

        T Get();
    }

    public interface ISink<in T>
    {
        void Push(T item);
    }
}
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Veeam.HomeWork.Common.FileStructure;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Decompressor
{
    // Reader class implementation, reads input compressed file
    // Compressed file structure is described in read.me file
    public class ReaderImpl : IGenericProcessor<int, WorkItem>
    {
        private readonly List<FileItem> _fileBlocks;
        private readonly FileStream _stream;

        public ReaderImpl(string fileName)
        {
            _stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            var header = _stream.ReadFileHeader();
            var dataPosition = _stream.Position;
            _stream.Seek(header.Offset, SeekOrigin.Begin);
            _fileBlocks = _stream.ReadFileFooter(header.Length).ToList();
            _stream.Seek(dataPosition, SeekOrigin.Begin);
        }

        public void Dispose()
        {
            _stream?.Dispose();
        }

        public WorkItem Process(int index)
        {
            if (index >= _fileBlocks.Count)
            {
                return null;
            }

            var item = _fileBlocks[index];
            _stream.Seek(item.Offset, SeekOrigin.Begin);
            var buffer = new byte[item.Length];
            _stream.Read(buffer);
            var workItem = new WorkItem(index, _fileBlocks.Count, index == _fileBlocks.Count - 1, buffer);
            return workItem;
        }
    }
}
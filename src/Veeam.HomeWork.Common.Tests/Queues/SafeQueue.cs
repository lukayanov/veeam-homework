using FluentAssertions;
using Veeam.HomeWork.Common.Pipeline.Queues;
using Xunit;

namespace Veeam.HomeWork.Common.Tests.Queues
{
    public class SafeQueue
    {
        [Fact]
        public void Safe_queue_does_not_order_sequence()
        {
            var bp = new BackPressureCounter(100);
            var queue = new SafeQueue<TestClass>(bp);
            queue.Push(new TestClass(2));
            queue.Push(new TestClass(1));
            queue.Push(new TestClass(0));
            queue.Get().Value.Should().Be(2);
            queue.Get().Value.Should().Be(1);
            queue.Get().Value.Should().Be(0);
        }

        [Fact]
        public void Safe_queue_does_not_wait_for_the_value()
        {
            var bp = new BackPressureCounter(100);
            var queue = new SafeQueue<TestClass>(bp);
            queue.Push(new TestClass(2));
            queue.Push(new TestClass(1));
            queue.Get().Value.Should().Be(2);
            queue.Push(new TestClass(0));
            queue.Get().Value.Should().Be(1);
            queue.Get().Value.Should().Be(0);
            queue.Get().Should().BeNull();
        }

        [Fact]
        public void Safe_queue_allows_gaps()
        {
            var bp = new BackPressureCounter(100);
            var queue = new SafeQueue<TestClass>(bp);
            queue.Push(new TestClass(3));
            queue.Push(new TestClass(1));
            queue.Push(new TestClass(0));
            queue.Get().Value.Should().Be(3);
            queue.Get().Value.Should().Be(1);
            queue.Get().Value.Should().Be(0);
            queue.Get().Should().BeNull();
        }

        [Fact]
        public void Safe_queue_sets_event_when_data_is_ready()
        {
            var bp = new BackPressureCounter(100);
            var queue = new SafeQueue<TestClass>(bp);
            queue.Push(new TestClass(2));
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(1));
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(0));
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Get();
            queue.Event.WaitOne(0).Should().BeFalse();
        }

        [Fact]
        public void Safe_queue_maintains_pb_counter()
        {
            var bp = new BackPressureCounter(4);
            var queue = new SafeQueue<TestClass>(bp);
            queue.Push(new TestClass(2));
            bp.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(1));
            bp.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(0));
            bp.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(3));
            bp.Event.WaitOne(0).Should().BeFalse();
        }
    }
}
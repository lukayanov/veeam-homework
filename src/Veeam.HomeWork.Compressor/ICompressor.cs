using System.Threading;

namespace Veeam.HomeWork.Compressor
{
    public interface ICompressor
    {
        bool Compress(string inputFile, string outputFile, CancellationToken token);
    }
}
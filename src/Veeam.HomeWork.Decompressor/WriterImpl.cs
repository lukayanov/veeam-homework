using System.IO;
using Serilog;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Decompressor
{
    // Writer class implementation, writes decompressed blocks to the output file 
    public class WriterImpl : IGenericProcessor<WorkItem, WorkItem>
    {
        private readonly string _fileName;
        private readonly ILogger _logger = Log.ForContext<WriterImpl>();
        private readonly FileStream _stream;
        private bool _finalBlockReceived;

        public WriterImpl(string fileName)
        {
            _fileName = fileName;
            _stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read);
        }

        public void Dispose()
        {
            _stream?.Dispose();
            if (!_finalBlockReceived)
            {
                _logger.Debug("Final block is not received before disposing, output file will be removed");
                File.Delete(_fileName);
            }
        }

        public WorkItem Process(WorkItem item)
        {
            _stream.Write(item.Block);
            if (item.IsFinal)
            {
                _logger.Debug("Final block is received");
                _finalBlockReceived = true;
            }

            return new WorkItem(item.SerialNumber, item.TotalCount, item.IsFinal);
        }
    }
}
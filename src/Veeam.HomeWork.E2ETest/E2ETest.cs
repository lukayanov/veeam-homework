using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Veeam.HomeWork.Common;
using Veeam.HomeWork.Compressor;
using Veeam.HomeWork.Decompressor;
using Xunit;

namespace Veeam.HomeWork.E2ETest
{
    public class E2ETest
    {
        private static IEnumerable<string> RandomStringGenerator(int numberOfStrings)
        {
            return Enumerable.Range(0, numberOfStrings).Select(i => Guid.NewGuid().ToString());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(100)]
        [InlineData(1000000)]
        public void CompressDecompressTest(int count)
        {
            var inputFileName = Path.GetTempFileName();
            using var _1 = new DisposeAction(() => File.Delete(inputFileName));
            var compressedFileName = Path.GetTempFileName();
            using var _2 = new DisposeAction(() => File.Delete(compressedFileName));
            var decompressedFileName = Path.GetTempFileName();
            using var _3 = new DisposeAction(() => File.Delete(decompressedFileName));

            using (var writer = new StreamWriter(inputFileName))
            {
                foreach (var line in RandomStringGenerator(count))
                {
                    writer.WriteLine(line);
                }
            }

            {
                var compressor = new Compressor.Compressor(Options.Create(new CompressorOptions {BlockSize = 0x100000, ProcessorThreadsCount = 10, MaxMessagesInQueues = 60}));
                var result = compressor.Compress(inputFileName, compressedFileName, CancellationToken.None);
                result.Should().BeTrue();
            }
            {
                var decompressor = new Decompressor.Decompressor(Options.Create(new DecompressorOptions {ProcessorThreadsCount = 10, MaxMessagesInQueues = 60}));
                var result = decompressor.DeCompress(compressedFileName, decompressedFileName, CancellationToken.None);
                result.Should().BeTrue();
            }
            FileCompare(inputFileName, decompressedFileName).Should().BeTrue();
        }

        private static bool FileCompare(string left, string right)
        {
            if (left == right)
            {
                throw new ArgumentException("Comparing file to itself ");
            }

            using var leftFs = new FileStream(left, FileMode.Open);
            using var rightFs = new FileStream(right, FileMode.Open);

            if (leftFs.Length != rightFs.Length)
            {
                return false;
            }

            while (true)
            {
                var leftByte = leftFs.ReadByte();
                var rightByte = rightFs.ReadByte();
                if (leftByte != rightByte)
                {
                    return false;
                }

                if (leftByte == -1)
                {
                    return true;
                }
            }
        }
    }
}
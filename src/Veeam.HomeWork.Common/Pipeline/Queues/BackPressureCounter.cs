using System;
using System.Threading;

namespace Veeam.HomeWork.Common.Pipeline.Queues
{
    // This object is used to limit the number of items across the queues in the pipeline 
    public class BackPressureCounter : IItemsCounter, IBackPressure
    {
        private readonly ManualResetEvent _event = new ManualResetEvent(true);
        private readonly int _maxItemsInTheQueues;
        private readonly object _sync = new object();
        private int _counter;

        public BackPressureCounter(int maxItemsInTheQueues)
        {
            _maxItemsInTheQueues = maxItemsInTheQueues;
        }

        public WaitHandle Event => _event;

        public void Increment()
        {
            lock (_sync)
            {
                ++_counter;
                if (_counter == _maxItemsInTheQueues)
                {
                    _event.Reset();
                }
            }
        }

        public void Decrement()
        {
            lock (_sync)
            {
                var value = _counter--;
                if (_counter < 0)
                {
                    throw new IndexOutOfRangeException("Back pressure counter is below zero.");
                }

                if (value == _maxItemsInTheQueues)
                {
                    _event.Set();
                }
            }
        }
    }
}
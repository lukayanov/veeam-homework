using System;
using System.Threading;
using Veeam.HomeWork.Common.Pipeline.Queues;

namespace Veeam.HomeWork.Common.Pipeline
{
    // The region below stops rider from reordering the fields below 

    #region do not reorder

    // This is the pipeline that is used for both compression and decompression.
    // The pipeline sets up the following topology: single reader -> one ore multiple processors-> single writer.
    // reader, writer and processors implement IGenericProcessor interface. Each IGenericProcessor object runs on dedicated thread.
    // Items are transferred from one IGenericProcessor to another through the queues.
    public class Pipeline
    {
        private readonly Action<long> _progressCallback;

        // Reader
        private readonly ThreadedProcessor<int, WorkItem> _threadedReader;
        private readonly IQueue<WorkResult<WorkItem>> _readerSink;

        // Processor
        private readonly ThreadedProcessor<WorkItem, WorkItem> _threadedProcessor;
        private readonly IQueue<WorkResult<WorkItem>> _processorSink;
        private readonly IQueue<WorkItem> _processorSource;

        // Writer
        private readonly ThreadedProcessor<WorkItem, WorkItem> _threadedWriter;
        private readonly IQueue<WorkResult<WorkItem>> _writerSink;
        private readonly IQueue<WorkItem> _writerSource;

        #endregion

        // IoC is not used here because this is a "solid" algorithm, not intended to be changed or reconfigured 
        // In any case IoC can be easily added here if needed
        public Pipeline(Func<IGenericProcessor<int, WorkItem>> readersFactory, Func<IGenericProcessor<WorkItem, WorkItem>> processorsFactory,
            Func<IGenericProcessor<WorkItem, WorkItem>> writersFactory, int processorsCount, int maxMessagesInQueue, Action<long> progressCallback)
        {
            _progressCallback = progressCallback;
            const int startIndex = 0;
            var backPressure = new BackPressureCounter(maxMessagesInQueue);
            {
                var readerSource = new SerialSource(backPressure, startIndex);
                _readerSink = new SafeQueue<WorkResult<WorkItem>>(backPressure);
                _threadedReader = new ThreadedProcessor<int, WorkItem>(readerSource, _readerSink, readersFactory, 1);
            }
            {
                _processorSource = new SafeQueue<WorkItem>(backPressure);
                _processorSink = new SafeQueue<WorkResult<WorkItem>>(backPressure);
                _threadedProcessor = new ThreadedProcessor<WorkItem, WorkItem>(_processorSource, _processorSink, processorsFactory, processorsCount);
            }
            {
                // sorting queue here because previous processor can run in multiple instances, thus the ordering must to be restored    
                _writerSource = new OrderedSafeQueue<WorkItem>(i => i.SerialNumber, backPressure, startIndex);
                _writerSink = new SafeQueue<WorkResult<WorkItem>>(backPressure);
                _threadedWriter = new ThreadedProcessor<WorkItem, WorkItem>(_writerSource, _writerSink, writersFactory, 1);
            }
        }

        public void Run(CancellationToken token)
        {
            using var _1 = _threadedReader.Start();
            using var _2 = _threadedProcessor.Start();
            using var _3 = _threadedWriter.Start();
            while (true)
            {
                token.ThrowIfCancellationRequested();
                var result = WaitHandle.WaitAny(new[] {_writerSink.Event, _processorSink.Event, _readerSink.Event}, Constants.Timeout);
                switch (result)
                {
                    case WaitHandle.WaitTimeout:
                        throw new TimeoutException("Timeout is expired waiting for sink event.");
                    case 0 when OnWriterEvent(): // Final mark is received
                        return;
                    case 1:
                        OnProcessorEvent();
                        break;
                    case 2:
                        OnReaderEvent();
                        break;
                }
            }
        }

        private void OnReaderEvent()
        {
            var result = _readerSink.Get();
            if (result.Ex != null)
            {
                throw new ReaderException(result.Ex);
            }

            _processorSource.Push(result.Item);
        }

        private void OnProcessorEvent()
        {
            var result = _processorSink.Get();
            if (result.Ex != null)
            {
                throw new ProcessorException(result.Ex);
            }

            _writerSource.Push(result.Item);
        }

        private bool OnWriterEvent()
        {
            var result = _writerSink.Get();
            if (result.Ex != null)
            {
                throw new WriterException(result.Ex);
            }

            ReportProgress(result);
            return result.Item.IsFinal;
        }

        private void ReportProgress(WorkResult<WorkItem> result)
        {
            var prevPercent = (result.Item.SerialNumber - 1) * 100 / result.Item.TotalCount;
            var currentPercent = result.Item.SerialNumber * 100 / result.Item.TotalCount;
            if (prevPercent != currentPercent)
            {
                _progressCallback(currentPercent);
            }
        }
    }
}
using FluentAssertions;
using Veeam.HomeWork.Common.Pipeline.Queues;
using Xunit;

namespace Veeam.HomeWork.Common.Tests.Queues
{
    public class SerialSourceTest
    {
        [Fact]
        public void Serial_source_produces_uninterrupted_sequence_of_integers()
        {
            var bp = new BackPressureCounter(1000);
            var queue = new SerialSource(bp, 0);
            for (var i = 0; i < 1000; ++i)
            {
                queue.Get().Should().Be(i);
            }
        }

        [Fact]
        public void Serial_source_maintains_backpressure()
        {
            var bp = new BackPressureCounter(2);
            var queue = new SerialSource(bp, 0);
            queue.Event.WaitOne(0).Should().BeTrue();
            bp.Increment();
            queue.Event.WaitOne(0).Should().BeTrue();
            bp.Increment();
            queue.Event.WaitOne(0).Should().BeFalse();
            bp.Decrement();
            queue.Event.WaitOne(0).Should().BeTrue();
        }

        [Fact]
        public void Serial_source_getter_does_not_affect_backpressure()
        {
            var bp = new BackPressureCounter(1);
            var queue = new SerialSource(bp, 0);
            queue.Get();
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Get();
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Get();
            queue.Event.WaitOne(0).Should().BeTrue();
        }
    }
}
namespace Veeam.HomeWork.Decompressor
{
    public class DecompressorOptions
    {
        public int ProcessorThreadsCount { get; set; }
        public int MaxMessagesInQueues { get; set; }
    }
}
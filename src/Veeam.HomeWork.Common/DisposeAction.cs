using System;

namespace Veeam.HomeWork.Common
{
    // A little helper allowing to execute some action at the end of the scope defined by using directive     
    public class DisposeAction : IDisposable
    {
        private readonly Action _action;

        public DisposeAction(Action action)
        {
            _action = action;
        }

        public void Dispose()
        {
            _action();
        }
    }
}
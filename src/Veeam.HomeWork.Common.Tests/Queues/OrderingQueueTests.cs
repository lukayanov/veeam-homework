using FluentAssertions;
using Veeam.HomeWork.Common.Pipeline.Queues;
using Xunit;

namespace Veeam.HomeWork.Common.Tests.Queues
{
    public class OrderingQueueTests
    {
        [Fact]
        public void Ordering_queue_orders_sequence()
        {
            var bp = new BackPressureCounter(100);
            var queue = new OrderedSafeQueue<TestClass>(t => t.Value, bp, 0);
            queue.Push(new TestClass(2));
            queue.Push(new TestClass(1));
            queue.Push(new TestClass(0));
            queue.Get().Value.Should().Be(0);
            queue.Get().Value.Should().Be(1);
            queue.Get().Value.Should().Be(2);
        }

        [Fact]
        public void Ordering_queue_waits_for_the_value()
        {
            var bp = new BackPressureCounter(100);
            var queue = new OrderedSafeQueue<TestClass>(t => t.Value, bp, 0);
            queue.Push(new TestClass(2));
            queue.Push(new TestClass(1));
            queue.Get().Should().BeNull();
            queue.Push(new TestClass(0));
            queue.Get().Value.Should().Be(0);
            queue.Get().Value.Should().Be(1);
            queue.Get().Value.Should().Be(2);
        }

        [Fact]
        public void Ordering_queue_does_not_allow_gaps()
        {
            var bp = new BackPressureCounter(100);
            var queue = new OrderedSafeQueue<TestClass>(t => t.Value, bp, 0);
            queue.Push(new TestClass(3));
            queue.Get().Should().BeNull();
            queue.Push(new TestClass(1));
            queue.Push(new TestClass(0));
            queue.Get().Value.Should().Be(0);
            queue.Get().Value.Should().Be(1);
            queue.Get().Should().BeNull();
        }

        [Fact]
        public void Ordering_queue_starts_from_null()
        {
            var bp = new BackPressureCounter(100);
            var queue = new OrderedSafeQueue<TestClass>(t => t.Value, bp, 0);
            queue.Push(new TestClass(2));
            queue.Push(new TestClass(1));
            queue.Get().Should().BeNull();
            queue.Push(new TestClass(0));
            queue.Get().Value.Should().Be(0);
            queue.Get().Value.Should().Be(1);
            queue.Get().Value.Should().Be(2);
        }

        [Fact]
        public void Ordering_queue_sets_event_when_data_is_ready()
        {
            var bp = new BackPressureCounter(100);
            var queue = new OrderedSafeQueue<TestClass>(t => t.Value, bp, 0);
            queue.Push(new TestClass(2));
            queue.Push(new TestClass(1));
            queue.Event.WaitOne(0).Should().BeFalse();
            queue.Push(new TestClass(0));
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Get();
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Get();
            queue.Event.WaitOne(0).Should().BeTrue();
            queue.Get();
            queue.Event.WaitOne(0).Should().BeFalse();
        }

        [Fact]
        public void Ordering_queue_maintains_pb_counter()
        {
            var bp = new BackPressureCounter(4);
            var queue = new OrderedSafeQueue<TestClass>(t => t.Value, bp, 0);
            queue.Push(new TestClass(2));
            bp.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(1));
            bp.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(0));
            bp.Event.WaitOne(0).Should().BeTrue();
            queue.Push(new TestClass(3));
            bp.Event.WaitOne(0).Should().BeFalse();
        }
    }
}
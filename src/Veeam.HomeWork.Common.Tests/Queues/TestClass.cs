namespace Veeam.HomeWork.Common.Tests.Queues
{
    public class TestClass
    {
        public TestClass(int v)
        {
            Value = v;
        }

        public int Value { get; set; }
    }
}
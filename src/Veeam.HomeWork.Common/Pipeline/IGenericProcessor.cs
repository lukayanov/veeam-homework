using System;

namespace Veeam.HomeWork.Common.Pipeline
{
    public interface IGenericProcessor<in TInput, out TOutput> : IDisposable
    {
        TOutput Process(TInput item);
    }
}
using System.Runtime.InteropServices;

namespace Veeam.HomeWork.Common.FileStructure
{
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public struct FileItem
    {
        public long Offset { get; set; }
        public long Length { get; set; }
    }
}
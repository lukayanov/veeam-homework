﻿using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Hosting;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Veeam.HomeWork.Compressor;
using Veeam.HomeWork.Decompressor;

namespace Veeam.HomeWork.GZipTest
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var logger = CreateLogger();
            // System.CommandLine.Hosting package is used here to build the app.
            // This package is still in alpha stage (0.3.0-alpha.20371.2), I believe it should not be a problem for a test project  
            var resultCode = BuildCommandLine()
                .UseHost(BuildHost)
                .UseDefaults()
                .CancelOnProcessTermination()
                .UseExceptionHandler((ex, c) =>
                {
                    c.ResultCode = -1;
                    logger.Fatal(ex, "Unexpected error has occured, aborting.");
                })
                .Build()
                .Invoke(args);
            return resultCode;
        }

        private static CommandLineBuilder BuildCommandLine()
        {
            var inputArgument = new Argument("input-file-name")
            {
                Arity = ArgumentArity.ExactlyOne,
                ArgumentType = typeof(string),
                Description = "Input file name"
            }.LegalFilePathsOnly();
            var outputArgument = new Argument("output-file-name")
            {
                Arity = ArgumentArity.ExactlyOne,
                ArgumentType = typeof(string),
                Description = "Output file name"
            };
            var blockSizeOption = new Option<int>(new[] {"--block-size", "-s"}, () => 0x100000)
            {
                IsRequired = false,
                Description = $"$Block size in bytes in the range [1024, {int.MaxValue}]"
            }.AllowedRange(1024, int.MaxValue);
            var threadsCount = new Option<int>(new[] {"--processor-threads-count", "-t"}, () => 20)
            {
                IsRequired = false,
                Description = $"Number of threads to be used for operation in the range [1, {int.MaxValue}"
            }.AllowedRange(1, int.MaxValue);
            var maxMessagesInQueuesCount = new Option<int>(new[] {"--max-messages-in-queues", "-m"}, () => 60)
            {
                IsRequired = false,
                Description = $"Number of threads to be used for operation in the range [1, {int.MaxValue}"
            }.AllowedRange(1, int.MaxValue);

            var builder = new CommandLineBuilder();
            {
                // In the help string (--help compress) arguments are incorrectly shown as optional: [<input-file-name> [<output-file-name>]], however these arguments behave as required ones.
                // Looks like a bug of alpha release 
                var compressCommand = new Command("compress") {inputArgument, outputArgument, blockSizeOption, threadsCount, maxMessagesInQueuesCount};
                compressCommand.Handler = CommandHandler.Create<IHost, string, string, CancellationToken>((host, inputFileName, outputFileName, token) =>
                {
                    var result = host.Services.GetRequiredService<ICompressor>().Compress(inputFileName, outputFileName, token);
                    return result ? 0 : 1;
                });
                builder.AddCommand(compressCommand);
            }
            {
                var decompressCommand = new Command("decompress") {inputArgument, outputArgument, threadsCount, maxMessagesInQueuesCount};
                decompressCommand.Handler = CommandHandler.Create<IHost, string, string, CancellationToken>((host, inputFileName, outputFileName, token) =>
                {
                    var result = host.Services.GetRequiredService<IDecompressor>().DeCompress(inputFileName, outputFileName, token);
                    return result ? 0 : 1;
                });
                builder.AddCommand(decompressCommand);
            }
            return builder;
        }

        private static ILogger CreateLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss} [{Level}] ({SourceContext}) {Message}{NewLine}{Exception}")
                .CreateLogger();
            return Log.ForContext(typeof(Program));
        }

        private static void BuildHost(IHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddOptions<DecompressorOptions>().BindCommandLine();
                services.AddOptions<CompressorOptions>().BindCommandLine();
                services.AddTransient<ICompressor, Compressor.Compressor>();
                services.AddTransient<IDecompressor, Decompressor.Decompressor>();
            }).UseSerilog();
        }
    }
}
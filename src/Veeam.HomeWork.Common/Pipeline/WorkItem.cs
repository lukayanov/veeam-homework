namespace Veeam.HomeWork.Common.Pipeline
{
    // Piece of work for the processor 
    public class WorkItem
    {
        public WorkItem(long serialNumber, long totalCount, bool isFinal, byte[] block = null)
        {
            IsFinal = isFinal;
            SerialNumber = serialNumber;
            TotalCount = totalCount;
            Block = block;
        }

        public bool IsFinal { get; }
        public long TotalCount { get; }
        public long SerialNumber { get; }
        public byte[] Block { get; }
    }
}
using System.Threading;

namespace Veeam.HomeWork.Common.Pipeline.Queues
{
    // Source only implementation used to inject messages in the pipeline
    // Maintains backpressure through signaling event
    public class SerialSource : ISource<int>
    {
        private readonly IBackPressure _backpressure;
        private int _counter;

        public SerialSource(IBackPressure backpressure, int startIndex)
        {
            _counter = startIndex;
            _backpressure = backpressure;
        }

        public int Get()
        {
            var value = Interlocked.Increment(ref _counter);
            return value - 1; // returning the value before increment
        }

        public WaitHandle Event => _backpressure.Event;

        // For debugging purpose only
        public override string ToString()
        {
            return $"Current value is {Interlocked.CompareExchange(ref _counter, 0, 0)}";
        }
    }
}
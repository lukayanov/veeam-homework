using System.IO;
using System.IO.Compression;
using Veeam.HomeWork.Common.Pipeline;

namespace Veeam.HomeWork.Compressor
{
    // Compression processor, compresses one byte array and stores it to another 
    public class ProcessorImpl : IGenericProcessor<WorkItem, WorkItem>
    {
        public WorkItem Process(WorkItem item)
        {
            using var inputStream = new MemoryStream(item.Block);
            using var outputStream = new MemoryStream();
            using (var compressionStream = new GZipStream(outputStream, CompressionMode.Compress))
            {
                inputStream.CopyTo(compressionStream);
            }

            return new WorkItem(item.SerialNumber, item.TotalCount, item.IsFinal, outputStream.ToArray());
        }

        public void Dispose()
        {
            // Nothing to dispose here
        }
    }
}
# README #

Тестовое задания для компании Veeam 

## Задание ##
Разработать консольное приложение на C# для поблочного сжатия и распаковки файлов с помощью System.IO.Compression.GzipStream.

Для сжатия исходный файл делится на блоки одинакового размера, например, в 1 мегабайт. Каждый блок сжимается и записывается в выходной файл независимо от остальных блоков.

Программа должна эффективно распараллеливать и синхронизировать обработку блоков в многопроцессорной среде и уметь обрабатывать файлы, размер которых превышает объем доступной оперативной памяти. 

В случае исключительных ситуаций необходимо проинформировать пользователя понятным сообщением, позволяющим пользователю исправить возникшую проблему, в частности, если проблемы связаны с ограничениями операционной системы.
При работе с потоками допускается использовать только базовые классы и объекты синхронизации (Thread, Manual/AutoResetEvent, Monitor, Semaphor, Mutex) и не допускается использовать async/await, ThreadPool, BackgroundWorker, TPL.
Код программы должен соответствовать принципам ООП и ООД (читаемость, разбиение на классы и т.д.). 

Параметры программы, имена исходного и результирующего файлов должны задаваться в командной строке следующим образом:
GZipTest.exe compress/decompress [имя исходного файла] [имя результирующего файла]
В случае успеха программа должна возвращать 0, при ошибке возвращать 1.

Примечание: формат архива остаётся на усмотрение автора, и не имеет значения для оценки качества тестового, в частности соответствие формату GZIP опционально.

Исходники необходимо прислать вместе с проектом Visual Studio.

## Решение ##
Проект исполнен на .Net Core 3.1 (моя текущая рабочая машина - Mac)
Исполняемый проект: Veeam.HomeWork.GZipTest.
Проект представляет собой консольное приложение, поддерживающее две команды compress/decompress. Каждая команда ожидает на вход два обязательных параметра: имя входного файл и имя выходного файла. Кроме того, доступны следующие опции:
- --block-size размер блока, используемого компрессором, по умолчанию 0x100000
- --processor-threads-count количество потоков используемых для компресии/декомпресии, по умолчанию 10
- --max-messages-in-queues лимит на количество блоков, одновременно находящихся в памяти, по умолчанию 60

Приложение имеет особенности поведения по сравнению с обычными архиваторами:
- выходной файл перезаписывается без уведомления пользователя
- пустой входной файл считается ошибкой


### Структура сжатого файла ###

Структура сжатого файла представлена в таблице ниже. Файл состоит из заголовка, тела и окончания.

Заголовок с фиксированной длиной в 16 байт содержит только смещение и размер окончания.

Сжатое тело расположено между заголовком и окончанием и состоит из последовательно сохраненных сжатых блоков. 

Окончание переменной длины содержит по одной 16-и байтной записи для каждого сжатого блока в теле файла. Каждая запись в окончании хранит смещение блока относительно начала файла и размер блока.

Средства контроля целостности (CRC) отсутствуют.

```
|----------|------------------------------------------|
|          | 8 bytes (long)  foooter offset           | 
|  header  | -----------------------------------------|
| 16 bytes | 8 bytes (long)  foooter length           |
|----------|------------------------------------------|
|   body   | N Bytes (N = footer offset -16)          |
|----------|------------------------------------------|
|          | 8 bytes (long) 1 compressed block offset |
| footer   | -----------------------------------------|
| X bytes, | 8 bytes (long) 1 compressed block offset |
| see      | -----------------------------------------|
| header   | 8 bytes (long) 2 compressed block offset |
|          | -----------------------------------------|
|          | 8 bytes (long) 2 compressed block offset |
|          | -----------------------------------------|
- - - - - - - - - - - - - - - - - - - - - - - - - - - |
|          | 8 bytes (long) N compressed block offset |
|          | -----------------------------------------|
|          | 8 bytes (long) N compressed block offset |
|----------| -----------------------------------------|
```

### Структура проекта ###

- **Veeam.HomeWork.GZipTest** - исполняемый проект, основное назначение это точка входа, парсинг аргументов, инициация основных компонентов и запуск компрессора или декомпрессора в зависимости от команды.

- **Veeam.HomeWork.Compressor**  - классы необходимые для реализации команды compress

- **Veeam.HomeWork.Decompressor**  - классы необходимые для реализации команды decompress

- **Veeam.HomeWork.Common** - классы, необходимые для обеих команд compress/decompress: пайплайн обработки данных, классы для задания структуры файла6 исключения.


### Pipeline ###

Pipeline представляет собой некоторую обвязку позволяющую собирать многопоточный обработчик файлов. Один и тот же пайплайн используется для обеих операций compress и decompress. Каждый pipeline параметризуется тремя процессорами, в данном случае ответственными за операции read, process, write. Каждый экземпляр  процессоров исполняется в своем потоке. При этом начинка процессоров изолирована от многопоточной среды вокруг процессора - каждый процессор исполняет элементарные действия без знания о других процессорах, о необходимости синхронизации и т.д.

Процессор read ответственен за чтение входного файла. read исполняется в одном экземпляре, в своем потоке.

Процессор process может исполняться в нескольких экземплярах, каждый экземпляр исполняется в своем потоке.

Процессор write записывает данные в выходной файл, исполняется в одном экземпляре в своем потоке. 

### Юнит тесты ###
Юнит тестирование в этом проекте сделано для иллюстрации потенциальной возможности тестирования. В продуктовом коде количество и качество тестов должно быть сильно выше, но тут и так слишком много кода для тестового задания.


